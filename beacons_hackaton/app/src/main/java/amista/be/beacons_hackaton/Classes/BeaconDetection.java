package amista.be.beacons_hackaton.Classes;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.provider.Settings.Secure;

import org.altbeacon.beacon.Beacon;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import amista.be.beacons_hackaton.R;
import amista.be.beacons_hackaton.WebServices.JsonParser;
import amista.be.beacons_hackaton.WebServices.WebCalls;


public class BeaconDetection extends AppCompatActivity { //implements ASScannerCallback,ASConDeviceCallback,ASEDSTCallback, ASiBeaconCallback, ASGlobalCallback{

    private String android_id;
    private Activity activity;

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothGatt mBluetoothGatt;
    private BluetoothLeScanner scanner;
    private ScanSettings scanSettings;

    private List<String> scannedDeivcesList;
    private List<LocalBeacon> scannedBeaconsData;

    private RetrievedData retrievedData;
    private Boolean isScanning = false;
    private Timer timer;

    private Object lock = new Object();

    private double maxDistance = 5;

    private ArrayAdapter<String> adapter;
    private ListView devicesList;
    private int n = RETRIEVE;

    private WebCalls webCalls = new WebCalls();

    private static String TAG = "BeaconDetection";
    private static String PREF = "Amista";
    private static int TIMEOUT = 10000; // Every 2 minutes there should be a check
    private static int RETRIEVE = 5000; // Retrieve data for x seconds, than send
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private static final int PERMISSION_REQUEST_COARSE_BL = 2;

    @TargetApi(23)
    private void checkLocBT(){
        //If Android version is M (6.0 API 23) or newer, check if it has Location permissions
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                //If Location permissions are not granted for the app, ask user for it! Request response will be received in the onRequestPermissionsResult.
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        //Check if permission request response is from Location
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //User granted permissions. Setup the scan settings
                    Log.d("TAG", "coarse location permission granted");
                } else {
                    //User denied Location permissions. Here you could warn the user that without
                    //Location permissions the app is not able to scan for BLE devices and eventually
                    //Close the app
                    finish();
                }
                return;
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beacon_detection);

        activity = this;

        //Define listview in layout
        devicesList = (ListView) findViewById(R.id.devicesList);

        //Inicialize de devices list
        scannedDeivcesList = new ArrayList<>();

        //Inicialize the list adapter for the listview with params: Context / Layout file / TextView ID in layout file / Devices list
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1, scannedDeivcesList);

        //Set the adapter to the listview
        devicesList.setAdapter(adapter);

        // Check if location can be used
        checkLocBT();
        //init Bluetooth adapter
        initBT();

        //Start scan of bluetooth devices
        setRetrieveData();
        timer = new Timer();
        SetTimer();

    }

    private void initBT(){
        final BluetoothManager bluetoothManager =  (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        //Create the scan settings
        ScanSettings.Builder scanSettingsBuilder = new ScanSettings.Builder();
        //Set scan latency mode. Lower latency, faster device detection/more battery and resources consumption
        scanSettingsBuilder.setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY);
        //Wrap settings together and save on a settings var (declared globally).
        scanSettings = scanSettingsBuilder.build();
        //Get the BLE scanner from the BT adapter (var declared globally)
        scanner = mBluetoothAdapter.getBluetoothLeScanner();
    }

    private void ResetBeacons(){
        retrievedData.clear();
        scannedDeivcesList.clear();
    }

    private void SetTimer()
    {
        timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {

                        long targetTime = System.currentTimeMillis() + RETRIEVE;
                        try {
                            startLeScan(true);
                            synchronized(lock) {
                                Log.i(TAG, "-- before");
                                while(System.currentTimeMillis() < targetTime){
                                    lock.wait(1000);
                                }
                                Log.i(TAG, "--after");
                            }

                        } catch (InterruptedException e) {
                            Log.i(TAG, "-- Error in timer");
                            Thread.currentThread().interrupt();
                        }

                        startLeScan(false);
                        sendData(System.currentTimeMillis());

                    }
                },
            //set the amount of time in milliseconds before first execution
            0,
            //Set the amount of time between each execution (in milliseconds)
            TIMEOUT);

    }

    private void startLeScan(boolean endis) {
        if (endis) {
            //********************
            //START THE BLE SCAN
            //********************
            //Scanning parameters FILTER / SETTINGS / RESULT CALLBACK. Filter are used to define a particular
            //device to scan for. The Callback is defined above as a method.

            ResetBeacons();

            scanner.startScan(null, scanSettings, mScanCallback);
        }else{
            //Stop scan
            scanner.stopScan(mScanCallback);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        startLeScan(false);
    }

    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);

            //Log.i(TAG, "-- Is Doing Callback " + System.currentTimeMillis());

            //Here will be received all the detected BLE devices around. "result" contains the device
            //address and name as a BLEPeripheral, the advertising content as a ScanRecord, the Rx RSSI
            //and the timestamp when received. Type result.get... to see all the available methods you can call.

            //Convert advertising bytes to string for a easier parsing. GetBytes may return a NullPointerException. Treat it right(try/catch).
            String advertisingString = byteArrayToHex(result.getScanRecord().getBytes());
            //Print the advertising String in the LOG with other device info (ADDRESS - RSSI - ADVERTISING - NAME)
            Log.i(TAG, result.getDevice().getAddress()+" - RSSI: "+result.getRssi()+"\t - "+advertisingString+" - "+result.getDevice().getName());
            double distance = getBeaconDistance(result);

            //Check if scanned device is already in the list by mac address
            boolean contains = false;
            for(int i=0; i<scannedDeivcesList.size(); i++){

                if(scannedDeivcesList.get(i).contains(result.getDevice().getAddress())){
                    if(distance > maxDistance){
                        scannedDeivcesList.remove(i);
                    }else {
                        //Device already added
                        contains = true;
                        //Replace the device with updated values in that position
                        scannedDeivcesList.set(i, getBeaconDistance(result) + "  " + result.getDevice().getName() + "\n       (" + result.getDevice().getAddress() + ")");
                        retrievedData.devices.set(i, getBeaconData(result));
                    }
                    break;
                }
            }

            if(!contains){
                //Scanned device not found in the list. NEW => add to list

                if(result.getDevice().getName() != null && result.getDevice().getName().contains(PREF) && distance < maxDistance) {
                    scannedDeivcesList.add(getBeaconDistance(result) + "  " + result.getDevice().getName() + "\n       (" + result.getDevice().getAddress() + ")");
                    addBeaconToData(getBeaconData(result));
                }else{
                    Log.i(TAG, "Device not added , name was " + result.getDevice().getName());
                }
            }

            //After modify the list, notify the adapter that changes have been made so it updates the UI.
            //UI changes must be done in the main thread
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.notifyDataSetChanged();
                }
            });
        }
    };



    private double getBeaconDistance(ScanResult result)
    {
        Log.i(TAG, "-- Distance " + result.getScanRecord().getTxPowerLevel() + " - " + result.getRssi());
        //return 10 ^ ( result.getScanRecord().getTxPowerLevel() - result.getRssi()) / (20);
       // return calculateDistance(2, result.getRssi());
        return calculateDistance(result.getScanRecord().getTxPowerLevel(), result.getRssi());
    }

    public double calculateDistance(int txPower, double rssi) {
        txPower = -58;

        if (rssi == 0) {
            return -1.0; // if we cannot determine accuracy, return -1.
        }

        double ratio = rssi*1.0/txPower;
        double distance;
        if (ratio < 1.0) {
            distance =  Math.pow(ratio,10);
        }
        else {
            distance =  (0.42093)*Math.pow(ratio,6.9476) +  0.54992;
        }
        Log.i(TAG, "avg mRssi: %" + rssi + " distance: " + distance);
        return Math.round(distance * 100.0) / 100.0 ;
    }

    private LocalBeacon getBeaconData(ScanResult result)
    {
        return new LocalBeacon(result.getDevice().getName(), result.getDevice().getAddress());
    }

    private void addBeaconToData(LocalBeacon localBeacon)
    {
           retrievedData.devices.add(localBeacon);
    }

    //Method to convert a byte array to a HEX. string.
    private String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for(byte b: a)
            sb.append(String.format("%02x", b & 0xff));
        return sb.toString();
    }

    private void setRetrieveData()
    {
        retrievedData = new RetrievedData(getDeviceId());
        retrievedData.deviceId = getDeviceId();
    }

    private void sendData(long timeStamp)
    {
        retrievedData.timeStamp = timeStamp;
        JsonParser parser = new JsonParser();
        JSONObject obj = parser.getJson(retrievedData);

        String jsonString = obj.toString();
        Log.i(TAG, "-- Output json is " + jsonString);

        try{
            String t = webCalls.excutePost(jsonString);
            Log.i(TAG, "Got call " + t);
        }
        catch(Exception e){
            Log.i(TAG, "sendData error " + e.toString());
        }

    }

    private String getDeviceId()
    {
        return Secure.getString(activity.getContentResolver(),
                Secure.ANDROID_ID);
    }

}
