package amista.be.beacons_hackaton.Classes;

import java.util.Date;

/**
 * Created by yro on 11/03/2017.
 */

public class LocalBeacon {
    public String Name;
    public String Address;

    public LocalBeacon(String tempName, String tempAddress)
    {
        Name = tempName;
        Address = tempAddress;
    }
}