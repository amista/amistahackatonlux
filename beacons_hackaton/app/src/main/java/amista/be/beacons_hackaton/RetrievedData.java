package amista.be.beacons_hackaton;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;

/**
 * Created by yro on 11/03/2017.
 */

public class RetrievedData {
    public String deviceId;
    public ArrayList<Beacon> devices;
    public long timeStamp;

    public RetrievedData(String deviceId)
    {
        deviceId = deviceId;
        devices = new ArrayList<Beacon>();
    }

    public void clear()
    {
        devices.clear();
        timeStamp = 0;
    }
}
