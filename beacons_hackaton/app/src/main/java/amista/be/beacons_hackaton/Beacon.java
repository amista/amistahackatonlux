package amista.be.beacons_hackaton;

import java.util.Date;

/**
 * Created by yro on 11/03/2017.
 */

public class Beacon{
    public String Name;
    public String Address;

    public Beacon(String name, String address)
    {
        Name = name;
        Address = address;
    }
}