package amista.be.beacons_hackaton.Classes;
import java.util.ArrayList;

/**
 * Created by yro on 11/03/2017.
 */

public class RetrievedData {
    public String deviceId;
    public ArrayList<LocalBeacon> devices;
    public long timeStamp;

    public RetrievedData(String deviceId)
    {
        deviceId = deviceId;
        devices = new ArrayList<LocalBeacon>();
    }

    public void clear()
    {
        devices.clear();
        timeStamp = 0;
    }
}
