package amista.be.beacons_hackaton.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import amista.be.beacons_hackaton.Classes.LocalBeacon;
import amista.be.beacons_hackaton.Classes.RetrievedData;

/**
 * Created by yro on 11/03/2017.
 */

public class JsonParser
{

    private static final String API_URL =
            "https://luxhackatonwarabffdc8e7.hana.ondemand.com/luxhackaton_war/rest/submitBeaconInfo";

    public JsonParser() {

    }

    public JSONObject getJson(RetrievedData data) {
        JSONObject jsonObj = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        try {
            jsonObj.put("DeviceId", data.deviceId);
            jsonObj.put("TimeStamp", data.timeStamp);

            for (LocalBeacon localBeacon : data.devices) {
                JSONObject obj = new JSONObject();
                obj.put("Name", localBeacon.Name);
                obj.put("Address", localBeacon.Address);
                jsonArray.put(obj);
            }

            jsonObj.put("Devices", jsonArray);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonObj;
    }

}
