package be.amista.dto;

public class Seat {
	public String seatId;
	public Boolean occupied;
	
	public Seat(String seatId, Boolean occupied) {
		super();
		this.seatId = seatId;
		this.occupied = occupied;
	}

	public String getSeatId() {
		return seatId;
	}
	
	public void setSeatId(String seatId) {
		this.seatId = seatId;
	}
	
	public Boolean getOccupied() {
		return occupied;
	}
	
	public void setOccupied(Boolean occupied) {
		this.occupied = occupied;
	}
}
