package be.amista.dto;

import java.util.ArrayList;

public class BeaconReceiverData {

	public String DeviceId;
	public ArrayList<Beacon> Devices;
	public Long TimeStamp;
	
	public String getDeviceId() {
		return DeviceId;
	}
	public void setDeviceId(String deviceId) {
		DeviceId = deviceId;
	}
	public ArrayList<Beacon> getDevices() {
		return Devices;
	}
	public void setDevices(ArrayList<Beacon> devices) {
		Devices = devices;
	}
	public Long getTimeStamp() {
		return TimeStamp;
	}
	public void setTimeStamp(Long timeStamp) {
		TimeStamp = timeStamp;
	}
	
	
}
