package be.amista.dto;

public class TempTrainRes {

	public Integer originPointId;
	public String originName;
	public String trainId;
	public String departureISO;
	public Integer delay;
	public String destination;
	public Integer destinationId;
	
	public Integer getOriginPointId() {
		return originPointId;
	}
	public void setOriginPointId(Integer originPointId) {
		this.originPointId = originPointId;
	}
	public String getOriginName() {
		return originName;
	}
	public void setOriginName(String originName) {
		this.originName = originName;
	}
	public String getTrainId() {
		return trainId;
	}
	public void setTrainId(String trainId) {
		this.trainId = trainId;
	}
	public String getDepartureISO() {
		return departureISO;
	}
	public void setDepartureISO(String departureISO) {
		this.departureISO = departureISO;
	}
	public Integer getDelay() {
		return delay;
	}
	public void setDelay(Integer delay) {
		this.delay = delay;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public Integer getDestinationId() {
		return destinationId;
	}
	public void setDestinationId(Integer destinationId) {
		this.destinationId = destinationId;
	}
	
	
}
