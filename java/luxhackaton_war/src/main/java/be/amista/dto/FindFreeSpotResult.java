package be.amista.dto;

import java.util.ArrayList;

public class FindFreeSpotResult {

	public Integer numberOfPeopleOnTheTrain;
	public ArrayList<Seat> seatInfo;
	public String yourSeatId;
	
	public Integer getNumberOfPeopleOnTheTrain() {
		return numberOfPeopleOnTheTrain;
	}
	public void setNumberOfPeopleOnTheTrain(Integer numberOfPeopleOnTheTrain) {
		this.numberOfPeopleOnTheTrain = numberOfPeopleOnTheTrain;
	}
	public ArrayList<Seat> getSeatInfo() {
		return seatInfo;
	}
	public void setSeatInfo(ArrayList<Seat> seatInfo) {
		this.seatInfo = seatInfo;
	}
	public String getYourSeatId() {
		return yourSeatId;
	}
	public void setYourSeatId(String yourSeatId) {
		this.yourSeatId = yourSeatId;
	}
}
