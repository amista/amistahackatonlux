package be.amista.utils;

import java.util.ArrayList;

import be.amista.dto.Seat;

public class OccupiedSeats {

	public static Boolean trueOrFalse(Integer numberOfPeopleLeft) {
		
		if (numberOfPeopleLeft == 0)
		{
			return false;
		}else {
			return true;
		}
	}
	
	public static ArrayList<Seat> getOccupiedSeatsForTrain(String trainId, Integer numberOfPeopleOnTrain)
	{
		Integer numberOfPeopleToAssign = numberOfPeopleOnTrain;
		
		//Return example data that could come from pressure sensors in the seats
		ArrayList<Seat> seatList = new ArrayList<Seat>();
		ArrayList<String> seats = new ArrayList<String>();
		
		seats.add("seatM1");
		seats.add("seatL3");
		seats.add("seatL12");
		seats.add("seatR10");
		seats.add("seatR12");
		seats.add("seatM2");
		seats.add("seatR11");
		seats.add("seatM3");
		seats.add("seatM5");
		seats.add("seatL11");
		seats.add("seatM6");
		seats.add("seatM7");
		seats.add("seatL9");
		seats.add("seatM8");
		seats.add("seatM9");
		seats.add("seatM10");
		seats.add("seatM11");
		seats.add("seatM12");
		seats.add("seatM13");
		seats.add("seatM14");
		seats.add("seatM15");
		seats.add("seatM16");
		seats.add("seatL1");
		seats.add("seatL4");
		seats.add("seatL5");
		seats.add("seatL6");
		seats.add("seatL7");
		seats.add("seatL8");
		seats.add("seatL10");
		seats.add("seatL13");
		seats.add("seatL14");
		seats.add("seatL15");
		seats.add("seatL16");
		seats.add("seatR1");
		seats.add("seatR2");
		seats.add("seatR3");
		seats.add("seatR4");
		seats.add("seatR5");
		seats.add("seatR6");
		seats.add("seatR7");
		seats.add("seatR8");
		seats.add("seatR9");
		seats.add("seatR13");
		seats.add("seatR14");
		seats.add("seatR15");
		seats.add("seatR16");
		seats.add("B1");
		seats.add("B2");
		seats.add("B3");
		seats.add("B4");
		seats.add("D1");
		seats.add("D2");
		seats.add("D3");
		seats.add("D4");
		seats.add("H2");
		seats.add("H3");
		seats.add("H4");
		
		for (String seatNumber : seats)
		{
			Seat seat = new Seat(seatNumber, trueOrFalse(numberOfPeopleToAssign));
			seatList.add(seat);
			
			if (seat.getOccupied())
			{
				numberOfPeopleToAssign--;
			}
		}
		
		//Special
		seatList.add(new Seat("seatM4", false));
		seatList.add(new Seat("seatL2", false));
		seatList.add(new Seat("H1", false));
		
		return seatList;
	}
}
