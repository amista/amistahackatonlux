package be.amista.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import be.amista.dto.TempPointRes;
import be.amista.dto.TempTrainRes;

public class GetData {

	public static ArrayList<TempTrainRes> getTrainsForAllStations() {
	
		ArrayList<TempTrainRes> resultData = new ArrayList<TempTrainRes>();
		ArrayList<TempPointRes> sourceData = getStationsOfLines();
		
		try {
			for (TempPointRes point : sourceData)
			{
				String url = "https://api.tfl.lu/v1/StopPoint/Departures/" + point.getPointId();
				System.out.println(url);
				JSONArray pointDetails = readJsonArrayFromUrl(url);
				
				for (int i = 0 ; i < pointDetails.length(); i++) {
			        JSONObject pointDetail = pointDetails.getJSONObject(i);
			        
			        //Check if it's a train
			        if (pointDetail.getString("type").equalsIgnoreCase("train"))
			        {
			        	TempTrainRes res = new TempTrainRes();
			        	res.setOriginPointId(point.getPointId());
			        	res.setOriginName(point.getName());
						res.setDelay(pointDetail.getInt("delay"));
						res.setDepartureISO(pointDetail.getString("departureISO"));
						res.setDestination(pointDetail.getString("destination"));
						Object aObj = pointDetail.get("destinationId");
						if(aObj instanceof Integer){
							res.setDestinationId(pointDetail.getInt("destinationId"));
						}else {
							res.setDestinationId(000000000);
						}
						res.setTrainId(pointDetail.getString("trainId"));
						
						resultData.add(res);
			        }
				}
			}
			
			return resultData;
		} catch (Exception e)
		{
			System.out.println(e.getLocalizedMessage());
			return null;
		}
	}
	
	public static ArrayList<TempPointRes> getStationsOfLines() {
		
		ArrayList<TempPointRes> resData = new ArrayList<TempPointRes>();
		
		ArrayList<String> data = new ArrayList<String>();
		data.add("2:C88---:CRE7100");
		data.add("2:C82---:RB5100");
		data.add("2:C82---:CRE1800");
		data.add("2:C82---:RB5900");
		data.add("2:C82---:RB3600");
		data.add("2:C82---:CRE6700");
		data.add("2:C82---:RB6200");
		data.add("2:C88---:RB7100");
		data.add("2:C82---:CRE88500");
		data.add("2:C82---:RB4700");
		data.add("2:C88---:IC18100");
		data.add("2:C82---:CRE3400");
		data.add("2:C87---:CRE88500");
		data.add("2:C88---:IC100");
		data.add("2:C82---:RB3700");
		data.add("2:C82---:IC4600");
		data.add("2:C82---:RB6300");
		data.add("2:C82---:RB8600");
		data.add("2:C82---:RB4800");
		
		for (String id : data)
		{
			//System.out.println("Fetching data for " + id);
			String url = "https://api.tfl.lu/v1/Line/" + id + "/StopPoints";
			
			try {
				JSONArray points = readJsonArrayFromUrl(url);
				
				//System.out.println("Number of points: " + points.length());
				for (int i = 0 ; i < points.length(); i++) {
			        Integer objPointId = (Integer)points.get(i);
			        
			        String urlStoppoints = "https://api.tfl.lu/v1/StopPoint/" + objPointId;
					JSONObject pointObj = readJsonFromUrl(urlStoppoints);
					if (pointObj != null)
					{
						JSONArray coordinatesArray = pointObj.getJSONObject("geometry").getJSONArray("coordinates");
						
						TempPointRes resultObject = new TempPointRes();
						resultObject.setId(id);
						resultObject.setName(pointObj.getJSONObject("properties").get("name").toString());
						resultObject.setCoordinates(""+coordinatesArray.get(0)+","+coordinatesArray.get(1));
						resultObject.setPointId(objPointId);
						
						resData.add(resultObject);
					}
			    }
				
				return resData;
				
			} catch (Exception e)
			{
				System.out.println(e.getLocalizedMessage());
				return null;
			}
		}
		
		return null;
	}
	
	private static String readAll(Reader rd) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = rd.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
	}
	
	public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
		
		try {
			// Create a trust manager that does not validate certificate chains
	        TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
	                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
	                    return null;
	                }
	                public void checkClientTrusted(X509Certificate[] certs, String authType) {
	                }
	                public void checkServerTrusted(X509Certificate[] certs, String authType) {
	                }
	            }
	        };
	 
	        // Install the all-trusting trust manager
	        SSLContext sc = SSLContext.getInstance("SSL");
	        sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	 
	        // Create all-trusting host name verifier
	        HostnameVerifier allHostsValid = new HostnameVerifier() {
	            public boolean verify(String hostname, SSLSession session) {
	                return true;
	            }
	        };
	 
	        // Install the all-trusting host verifier
	        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			
		    InputStream is = new URL(url).openStream();
		    try {
		      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
		      String jsonText = readAll(rd);
		      JSONObject json = new JSONObject(jsonText);
		      return json;
		    } finally {
		      is.close();
		    }
		}catch (Exception e)
		{
			System.out.println(e.getLocalizedMessage());
			return null;
		}
	 }
	
	public static JSONArray readJsonArrayFromUrl(String url) throws IOException, JSONException {
		
		try {
			// Create a trust manager that does not validate certificate chains
	        TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
	                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
	                    return null;
	                }
	                public void checkClientTrusted(X509Certificate[] certs, String authType) {
	                }
	                public void checkServerTrusted(X509Certificate[] certs, String authType) {
	                }
	            }
	        };
	 
	        // Install the all-trusting trust manager
	        SSLContext sc = SSLContext.getInstance("SSL");
	        sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	 
	        // Create all-trusting host name verifier
	        HostnameVerifier allHostsValid = new HostnameVerifier() {
	            public boolean verify(String hostname, SSLSession session) {
	                return true;
	            }
	        };
	 
	        // Install the all-trusting host verifier
	        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			
		    InputStream is = new URL(url).openStream();
		    try {
		      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
		      String jsonText = readAll(rd);
		      JSONArray json = new JSONArray(jsonText);
		      return json;
		    } finally {
		      is.close();
		    }
		}catch (Exception e)
		{
			System.out.println(e.getLocalizedMessage());
			return null;
		}
	 }
}
