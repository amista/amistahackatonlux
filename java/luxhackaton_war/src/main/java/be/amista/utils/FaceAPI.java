package be.amista.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONArray;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sap.core.connectivity.api.DestinationFactory;
import com.sap.core.connectivity.api.http.HttpDestination;

public class FaceAPI {

	private static final Logger LOGGER = LoggerFactory.getLogger(FaceAPI.class);
	public static final String microsoftCognitiveServicesApiKey = "8ba73a97e12e46ddb06d6af2b354c331";
	public static final String hcpDestinationName = "MICROSOFT";
	
	public static Integer processUrl(String url) {
		
		try {
			Context ctx = new InitialContext();
			DestinationFactory destinationFactory = (DestinationFactory) ctx.lookup(DestinationFactory.JNDI_NAME);             
			HttpDestination destination = (HttpDestination) destinationFactory.getDestination(hcpDestinationName);
			
			HttpClient client = destination.createHttpClient();
			HttpPost postMethod = new HttpPost("/face/v1.0/detect");
			postMethod.setHeader("Content-Type", "application/json");
			postMethod.setHeader("Ocp-Apim-Subscription-Key", microsoftCognitiveServicesApiKey);
            
			JSONObject obj = new JSONObject();
	        obj.put("url", url);
			
	        HttpEntity entity = new ByteArrayEntity(obj.toString().getBytes("UTF-8"));
   	     	postMethod.setEntity(entity);
   	     	
   	     	HttpResponse response = client.execute(postMethod);
	     	LOGGER.debug("Response Code : " + response.getStatusLine().getStatusCode());
	     	BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

	     	StringBuffer result = new StringBuffer();
	     	String line = "";
	     	while ((line = rd.readLine()) != null) {
	     		result.append(line);
	     	}
	     	
	     	LOGGER.debug(result.toString());
	     	JSONArray jsonArray = new JSONArray(result.toString()); 
		    return jsonArray.length();
	     	
		} catch (Exception e)
		{
			LOGGER.error(e.getLocalizedMessage());
		}
		
		return null;
	}
}
