package be.amista.resource;

import java.io.File;
import java.io.FileReader;
import java.net.HttpURLConnection;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import be.amista.dto.BeaconReceiverData;

@Path("/submitBeaconInfo")
@Produces({ MediaType.APPLICATION_JSON })
public class SubmitBeaconInfoService {
	
	@Context
	private HttpServletRequest servletRequest;
	
	private final Logger LOGGER = LoggerFactory.getLogger(SubmitBeaconInfoService.class);
	
	@POST
	public Response submitBeaconInfo (BeaconReceiverData beaconData) {
		
		LOGGER.debug(beaconData.getDeviceId());
		LOGGER.debug("Number of devices: " + beaconData.getDevices().size());
		
		CacheControl cc = new CacheControl();
		cc.setNoCache(true);
		
        try
        {
        	JSONObject beaconJSON = new JSONObject( beaconData );
        	System.out.println(beaconJSON.toString());
        	
        	//Parse the previous beacon data
        	JSONParser parser = new JSONParser();
        	File file = new File("beacondata.json");
        	Object obj = null;
    		if (file.exists()) {
        		FileReader reader = new FileReader("beacondata.json");
        		obj = parser.parse(reader);
        	}
        	JSONArray previousBeaconData;
        	if (obj == null)
        	{
        		previousBeaconData = new JSONArray();
        	}else {
        		previousBeaconData = new JSONArray(obj.toString());
        	}
        	
        	//Check if current deviceId already exists
        	System.out.println("Number of previous beacons: " + previousBeaconData.length());
        	for (int i = 0; i < previousBeaconData.length(); i++)
        	{
        		JSONObject jsonObj = previousBeaconData.getJSONObject(i);
        		
        		if (jsonObj.getString("deviceId").equalsIgnoreCase(beaconData.getDeviceId()))
        		{
        			previousBeaconData.remove(i);
        			break;
        		}
        	}
        	
        	previousBeaconData.put(beaconJSON);
        	
        	FileUtils.writeStringToFile(new File("beacondata.json"), previousBeaconData.toString());
        	
        	ResponseBuilder responseBuilder = Response.ok(previousBeaconData);
    		responseBuilder.cacheControl(cc);
    		return responseBuilder.build();
        }
        catch (Exception e)
        {
            LOGGER.error(e.getLocalizedMessage());
            ResponseBuilder responseBuilder = Response.status(HttpURLConnection.HTTP_INTERNAL_ERROR).entity(e.getLocalizedMessage());
			responseBuilder.cacheControl(cc);
			return responseBuilder.build();
        }
	}
}
