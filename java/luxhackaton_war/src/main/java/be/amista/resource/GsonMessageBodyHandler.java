package be.amista.resource;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import be.alcopa.entities.JsonIgnore;

@Provider
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@SuppressWarnings("nls")
public class GsonMessageBodyHandler<T> implements MessageBodyWriter<T>, MessageBodyReader<T> {

	private Gson gson;

	private synchronized Gson getGson() {
		if (gson == null) {
			final GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.setExclusionStrategies(new GsonExclusionStrategy());

			gsonBuilder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
				public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
						throws JsonParseException {
					String date = json.getAsJsonPrimitive().getAsString();
					return new Date(Long.parseLong(date));
				}
			});
			gsonBuilder.registerTypeAdapter(Date.class, new JsonSerializer<Date>() {
				public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
					return new JsonPrimitive(Long.toString(src.getTime()));
				}
			});
			gson = gsonBuilder.create();
		}
		return gson;
	}

	public boolean isWriteable(Class<?> arg0, Type arg1, Annotation[] arg2, MediaType arg3) {
		return true;
	}

	public boolean isReadable(Class<?> arg0, Type arg1, Annotation[] arg2, MediaType arg3) {
		return true;
	}

	public long getSize(T arg0, Class<?> arg1, Type arg2, Annotation[] arg3, MediaType arg4) {
		return -1;
	}

	public T readFrom(Class<T> clazz, Type type, Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, String> httpHeaders, InputStream content) throws IOException,
			WebApplicationException {
		Reader reader = new BufferedReader(new InputStreamReader(content, "UTF-8"));
		try {
			return getGson().fromJson(reader, type);
		} catch (JsonParseException e) {
			throw new WebApplicationException(e, Status.BAD_REQUEST);
		} finally {
			reader.close();
		}
	}

	public void writeTo(T object, Class<?> clazz, Type type, Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, Object> httpHeaders, OutputStream content) throws IOException,
			WebApplicationException {
		Writer writer = new BufferedWriter(new OutputStreamWriter(content, "UTF-8"));
		try {
			getGson().toJson(object, type, writer);
		} finally {
			writer.close();
		}
	}

	private static class GsonExclusionStrategy implements ExclusionStrategy {

		public boolean shouldSkipClass(Class<?> clazz) {
			return false;
		}

		public boolean shouldSkipField(FieldAttributes field) {
			if (field.getAnnotation(JsonIgnore.class) != null) {
				return true;
			} else {
				return false;
			}
		}

	}
}
