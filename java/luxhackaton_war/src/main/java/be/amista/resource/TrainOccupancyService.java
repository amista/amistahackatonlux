package be.amista.resource;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import be.amista.dto.FindFreeSpotResult;
import be.amista.utils.OccupiedSeats;

@Path("/trainOccupancy")
@Produces({ MediaType.APPLICATION_JSON })
public class TrainOccupancyService {

	@Context
	private HttpServletRequest servletRequest;
	
	private final Logger LOGGER = LoggerFactory.getLogger(TrainOccupancyService.class);
	
	@GET
	@Path("{trainId}/{preferredSeat}")
	public Response findFreeSpotForTrainId(@PathParam("trainId") String trainId, @PathParam("preferredSeat") String preferredSeat) {
		
		CacheControl cc = new CacheControl();
		cc.setNoCache(true);
		
		//seatPreference is window, aisle, disabled or ALL
		LOGGER.debug("TrainId: " + trainId);
		LOGGER.debug("Preferred Seat: " + preferredSeat);
		
		FindFreeSpotResult result = new FindFreeSpotResult();
		if (preferredSeat.equalsIgnoreCase("window"))
		{
			result.setYourSeatId("seatL2");
		}else if (preferredSeat.equalsIgnoreCase("aisle"))
		{
			result.setYourSeatId("seatM4");
		}else if (preferredSeat.equalsIgnoreCase("disabled"))
		{
			result.setYourSeatId("H1");
		}
		
		//Parse the previous beacon data
		try {
			JSONParser parser = new JSONParser();
	    	File file = new File("beacondata.json");
	    	Object obj = null;
			if (file.exists()) {
	    		FileReader reader = new FileReader("beacondata.json");
	    		obj = parser.parse(reader);
	    		
	    		if (obj != null)
	        	{
	    			//Add all unique beaconAddressess to Array
	    			ArrayList<String> uniqueBeacons = new ArrayList<String>();
	        		JSONArray previousBeaconData = new JSONArray(obj.toString());
	        		for (int i = 0; i < previousBeaconData.length(); i++)
	            	{
	            		JSONObject jsonObj = previousBeaconData.getJSONObject(i);
	            		JSONArray devices = jsonObj.getJSONArray("devices");
	            		for (int j = 0; j < devices.length(); j++)
	            		{
	            			JSONObject device = devices.getJSONObject(j);
	            			if (!uniqueBeacons.contains(device.getString("address")))
	            			{
	            				uniqueBeacons.add(device.getString("address"));
	            			}
	            		}
	            	}
	        		
	        		result.setNumberOfPeopleOnTheTrain(uniqueBeacons.size());
	        	}
	    	}
		} catch (Exception e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		
		if (result.getNumberOfPeopleOnTheTrain() == null)
		{
			result.setNumberOfPeopleOnTheTrain(0);
		}
			
		result.setSeatInfo(OccupiedSeats.getOccupiedSeatsForTrain(trainId, result.getNumberOfPeopleOnTheTrain()));
		
		ResponseBuilder responseBuilder = Response.ok(result);
		responseBuilder.cacheControl(cc);
		return responseBuilder.build();
	}
}
