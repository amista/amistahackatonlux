package be.amista.resource;

import java.net.HttpURLConnection;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import be.amista.dto.TempPointRes;
import be.amista.dto.TempTrainRes;
import be.amista.utils.GetData;

@Path("/tempService")
@Produces({ MediaType.APPLICATION_JSON })
public class TempService {

	@Context
	private HttpServletRequest servletRequest;
	
	private final Logger LOGGER = LoggerFactory.getLogger(TempService.class);
	
	@GET
	public Response formatData() {
		
		CacheControl cc = new CacheControl();
		cc.setNoCache(true);
		
		ArrayList<TempPointRes> stations = GetData.getStationsOfLines();
		
		if (stations != null)
		{
			ResponseBuilder responseBuilder = Response.ok(stations);
			responseBuilder.cacheControl(cc);
			return responseBuilder.build();
		}else {
			ResponseBuilder responseBuilder = Response.status(HttpURLConnection.HTTP_INTERNAL_ERROR);
			responseBuilder.cacheControl(cc);
			return responseBuilder.build();
		}
	}
	
	@GET
	@Path("trains")
	public Response formatDataTrains() {
		
		CacheControl cc = new CacheControl();
		cc.setNoCache(true);
		
		ArrayList<TempTrainRes> trains = GetData.getTrainsForAllStations();
		
		if (trains != null)
		{
			ResponseBuilder responseBuilder = Response.ok(trains);
			responseBuilder.cacheControl(cc);
			return responseBuilder.build();
		}else {
			ResponseBuilder responseBuilder = Response.status(HttpURLConnection.HTTP_INTERNAL_ERROR);
			responseBuilder.cacheControl(cc);
			return responseBuilder.build();
		}
	}
	
}
