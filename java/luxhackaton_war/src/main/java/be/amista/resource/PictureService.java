package be.amista.resource;

import java.net.HttpURLConnection;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import be.amista.utils.FaceAPI;

@Path("/processImage")
@Produces({ MediaType.APPLICATION_JSON })
public class PictureService {

	@Context
	private HttpServletRequest servletRequest;
	
	private final Logger LOGGER = LoggerFactory.getLogger(PictureService.class);
	
	@GET
	public Response dummyGetService () {
		
		CacheControl cc = new CacheControl();
		cc.setNoCache(true);
		
		ResponseBuilder responseBuilder = Response.ok("GET OK");
		responseBuilder.cacheControl(cc);
		return responseBuilder.build();
	}
	
	@POST
	public Response processImage (String urlString) {
		
		LOGGER.debug(urlString);
		
		CacheControl cc = new CacheControl();
		cc.setNoCache(true);
		
        try
        {
        	if (urlString.length() > 0)
        	{
        		Integer nrOfPeopleInPicture = FaceAPI.processUrl(urlString);
        		ResponseBuilder responseBuilder = Response.ok("People in the picture: " + nrOfPeopleInPicture);
        		responseBuilder.cacheControl(cc);
        		return responseBuilder.build();
        	}else {
        		ResponseBuilder responseBuilder = Response.status(HttpURLConnection.HTTP_INTERNAL_ERROR).entity("Empty url");
    			responseBuilder.cacheControl(cc);
    			return responseBuilder.build();
        	}
        }
        catch (Exception e)
        {
            LOGGER.error(e.getLocalizedMessage());
            ResponseBuilder responseBuilder = Response.status(HttpURLConnection.HTTP_INTERNAL_ERROR).entity(e.getLocalizedMessage());
			responseBuilder.cacheControl(cc);
			return responseBuilder.build();
        }
	}
}
