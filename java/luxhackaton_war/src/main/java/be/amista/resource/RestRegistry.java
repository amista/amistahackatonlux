package be.amista.resource;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

public class RestRegistry extends Application {

	private Set<Object> singletons = new HashSet<Object>();

	public RestRegistry() {
		singletons.add(new GsonMessageBodyHandler<Object>());
		singletons.add(new PictureService());
		singletons.add(new TrainOccupancyService());
		singletons.add(new SubmitBeaconInfoService());
		singletons.add(new TempService());
	}
	
	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}
