import base64
import os
import datetime
import requests
import json
import pysftp
from picamera import PiCamera
from time import sleep

camera = PiCamera()

def take_pic(camera):    
    camera.start_preview()
    sleep(2)
    camera.capture('/home/pi/Desktop/cam.jpg')
    camera.stop_preview()
    pass

def upload_pic():
    url = "https://luxhackatonwarabffdc8e7.hana.ondemand.com/luxhackaton_war/rest/processImage"
    imageLink = "http://appdistribution.appforce.be/imageshackaton/"
    
    currentDateTime = datetime.datetime.now()

    image_origin = '/home/pi/Desktop/cam.jpg'

    os.rename(image_origin,
              "/home/pi/Desktop/cam_{0}.jpg".format(currentDateTime)) 

    image_renamed = '/home/pi/Desktop/cam_{0}.jpg'.format(currentDateTime)    
    imageLink = '{0}cam_{1}.jpg'.format(imageLink, currentDateTime) 

    #image_64 = base64.encodestring(open(image, "rb").read())

    server = pysftp.Connection("212.71.247.65",
                               username="root", password="vpwRdmnU8Wtbq?x,Yh8t")
    with server.cd('/var/www/appdistribution/imageshackaton'):
        server.put(image_renamed)
    server.close()

    os.rename(image_renamed, image_origin)

    # send image url to service
    #data = {"body":"{0}".format(imageLink)}
    #data_json = json.dumps(data)
    headers = {'Content-type':'text/plain'}
    response = requests.post(url, data=imageLink, headers=headers)    
    print (response.json())
    pass    

while True:
    take_pic(camera)
    upload_pic()
    sleep(60)   # in seconds

#data = {"base64":"{0}".format(image_64)}
#data_json = json.dumps(data)
#headers = {'Content-type':'application/json'}
#response = requests.post(url, data=data, headers=headers)

#print (response)
    
os._exit(0)
