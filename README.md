# Amista Smart Railway App

## Project overview

### Beacons hackaton
Android Studio project. This code simulates sensors by using Android phone to capture BLE signals. Can be run on multiple android devices scattered in the train to provide full coverage. These devices detect tickets (tiny beacons) that travelers are carrying with them. 
They then send the different ID's from these tickets to the backend. 

### Java 
Java app based on SAP Cloud Platform. Reformats OpenData according to our needs and collects sensor data. Provides an API for the mobile app.

### Python
Code for the cameras in the train. Pictures are taken periodically which are used to count people through Microsoft Cognitive services.

### Web
Mobile web app in SAP UI5 for the passengers.