sap.ui.define(["sap/ui/core/mvc/Controller",
    "sap/m/MessageBox",
    "sap/ui/core/routing/History"
    ], function(BaseController, MessageBox, History) {
    "use strict";

    return BaseController.extend("generated.app.controller.dc8bce7fdd9bd95e0d87079c2_S2", {

     onAfterRendering: function() {
        var bindingParameters;
        

        var filterData;
        
     },

    manageFilters: function(controlId, aggregationName, filterData) {
       var view = this.getView();
       var filters = [];
       filterData.filters.forEach(function (filter) {
           var value1 = filter.type === 'Date' || filter.type === 'DateTime' ? new Date(filter.value1) : filter.value1;
           var value2 = null;
           if (filter.value2) {
               value2 = filter.type === 'Date' || filter.type === 'DateTime' ? new Date(filter.value2) : filter.value2;
           }
           var oFilter = new sap.ui.model.Filter(filter.path, filter.operator, value1, value2);
           filters.push(oFilter);
       });
       if (filterData.bindingParameters) {
         filterData.bindingParameters.filters = filters;
         view.byId(controlId).bindAggregation(aggregationName, filterData.bindingParameters);
       }
       else {
         view.byId(controlId).getBinding(aggregationName).filter(filters);
       }
    },



    onInit: function() {
        this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        this.oRouter.getTarget("dc8bce7fdd9bd95e0d87079c2_S2").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));

         


    },
	handleRouteMatched: function(oEvent) {
		var params = {};
        if (oEvent.mParameters.data.context || oEvent.mParameters.data.masterContext) {
            var oModel = this.getView ? this.getView().getModel() : null;
            if (oModel) {
                oModel.setRefreshAfterChange(true);

                if (oModel.hasPendingChanges()) {
                    oModel.resetChanges();
                }
            }

            this.sContext = oEvent.mParameters.data.context;
            this.sMasterContext = oEvent.mParameters.data.masterContext;

            if(!this.sContext) {
                this.getView().bindElement("/" + this.sMasterContext, params);
            }
            else {
                this.getView().bindElement("/" + this.sContext, params);
            }

        }

	},
	_onButtonPress2: function() {
		var oHistory = History.getInstance();
		var sPreviousHash = oHistory.getPreviousHash();
		
		if (sPreviousHash !== undefined) {
		    window.history.go(-1);
		} else {
		    var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
		    oRouter.navTo("default", true);
		}
		
	}
    });
}, /* bExport= */true);