sap.ui.define(["sap/ui/core/mvc/Controller",
    "sap/m/MessageBox",
    "sap/ui/core/routing/History"
    ], function(BaseController, MessageBox, History) {
    "use strict";

    return BaseController.extend("generated.app.controller.47806ee46b0d38480d86ffb21_S1", {

     onAfterRendering: function() {
        var bindingParameters;
        

        var filterData;
        
     },

    manageFilters: function(controlId, aggregationName, filterData) {
       var view = this.getView();
       var filters = [];
       filterData.filters.forEach(function (filter) {
           var value1 = filter.type === 'Date' || filter.type === 'DateTime' ? new Date(filter.value1) : filter.value1;
           var value2 = null;
           if (filter.value2) {
               value2 = filter.type === 'Date' || filter.type === 'DateTime' ? new Date(filter.value2) : filter.value2;
           }
           var oFilter = new sap.ui.model.Filter(filter.path, filter.operator, value1, value2);
           filters.push(oFilter);
       });
       if (filterData.bindingParameters) {
         filterData.bindingParameters.filters = filters;
         view.byId(controlId).bindAggregation(aggregationName, filterData.bindingParameters);
       }
       else {
         view.byId(controlId).getBinding(aggregationName).filter(filters);
       }
    },

	onInit: function() {
        this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        this.oRouter.getTarget("47806ee46b0d38480d86ffb21_S1").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
    },
    
	handleRouteMatched: function(oEvent) {
		var params = {};
        if (oEvent.mParameters.data.context || oEvent.mParameters.data.masterContext) {
            var oModel = this.getView ? this.getView().getModel() : null;
            if (oModel) {
                oModel.setRefreshAfterChange(true);

                if (oModel.hasPendingChanges()) {
                    oModel.resetChanges();
                }
            }

            this.sContext = oEvent.mParameters.data.context;
            this.sMasterContext = oEvent.mParameters.data.masterContext;

            if(!this.sContext) {
                this.getView().bindElement("/" + this.sMasterContext, params);
            }
            else {
                this.getView().bindElement("/" + this.sContext, params);
            }
        }
	},
	
	_onStationChange: function(oEvent) {
		var value = oEvent.getSource()._lastValue;
		var oFilter = new sap.ui.model.Filter("originName", sap.ui.model.FilterOperator.Contains, value);
		var allFilters = new sap.ui.model.Filter([oFilter], false);
		var table = this.byId("sap_Responsive_Page_0-content-build_simple_Table-1489275485512");
		var oBinding = table.getBinding("items");
		oBinding.filter(allFilters);
	},
	
	_onButtonPress1: function(oEvent) {
		var oBindingContext = oEvent.getSource().getBindingContext();
		
		return new ES6Promise.Promise(function(resolve, reject) {
		
		    this.doNavigate("dc8bce7fdd9bd95e0d87079c2_S2", oBindingContext, resolve, ""
		    );
		}.bind(this));
		
	},
	doNavigate: function(sRouteName, oBindingContext, fnPromiseResolve, sViaRelation) {
		var that = this;
		var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
		var oModel = (oBindingContext) ? oBindingContext.getModel() : null;
		
		var entityNameSet;
		if (sPath !== null && sPath !== "") {
		
		    if (sPath.substring(0, 1) === "/") {
		        sPath = sPath.substring(1);
		    }
		    entityNameSet = sPath.split("(")[0];
		}
		var navigationPropertyName;
		var sMasterContext = this.sMasterContext ? this.sMasterContext : sPath;
		
		if (entityNameSet !== null) {
		    navigationPropertyName = sViaRelation || that.getOwnerComponent().getNavigationPropertyForNavigationWithContext(entityNameSet, sRouteName);
		}
		if (navigationPropertyName !== null && navigationPropertyName !== undefined) {
		    if (navigationPropertyName === "") {
		        this.oRouter.navTo(sRouteName, {
		            context: sPath,
		            masterContext: sMasterContext
		        }, false);
		    } else {
		        oModel.createBindingContext(navigationPropertyName, oBindingContext, null, function (bindingContext) {
		            if (bindingContext) {
		                sPath = bindingContext.getPath();
		                if (sPath.substring(0, 1) === "/") {
		                    sPath = sPath.substring(1);
		                }
		            }
		            else {
		                sPath = "undefined";
		            }
		
		            // If the navigation is a 1-n, sPath would be "undefined" as this is not supported in Build
		            if (sPath === "undefined") {
		                that.oRouter.navTo(sRouteName);
		            } else {
		                that.oRouter.navTo(sRouteName, {
		                    context: sPath,
		                    masterContext: sMasterContext
		                }, false);
		            }
		        });
		    }
		} else {
		    this.oRouter.navTo(sRouteName);
		}
		
		if (typeof fnPromiseResolve === "function") {
		    fnPromiseResolve();
		}
		
	},
	_onRowPress: function(oEvent) {
		var oBindingContext = oEvent.getSource().getBindingContext();
		
		return new ES6Promise.Promise(function(resolve, reject) {
		
		    this.doNavigate("82b7b18c906b4a8d0d8756624_S4", oBindingContext, resolve, ""
		    );
		}.bind(this));
		
	}
    });
}, /* bExport= */true);